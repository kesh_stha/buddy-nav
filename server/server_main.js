// Load the http module to create an http server.
var http = require('http');
var https = require('https');

// https server: JS_Server.pdf page 38+39
// source/tutorial: http://coenraets.org/blog/2012/10/creating-a-rest-api-using-node-js-express-and-mongodb/

// no POST requests -> http://stackoverflow.com/questions/4508198/how-to-use-type-post-in-jsonp-ajax-call
// keep alive -> http://stackoverflow.com/questions/17942505/node-js-setinterval-connection-lost-the-server-closed-the-connection

// HTTPS -> http://stackoverflow.com/questions/5998694/how-to-create-an-https-server-in-node-js
// certificates -> http://docs.nodejitsu.com/articles/HTTP/servers/how-to-create-a-HTTPS-server

// test requests
/*
http://192.168.1.103:8080/getPositionOfUser?phoneNo=017655422803
http://ajs.jumpingcrab.com:8080/createNewUser?phoneNo=017655422803
http://ajs.jumpingcrab.com:8080/test
http://ajs.jumpingcrab.com:8080/getUserRegistrationStatus?phoneNo=017655422803
*/

user = require('./user');
var express = require('express');
var server = express();
var fs = require('fs');

var options = {
  key: fs.readFileSync('cert/key.pem'),
  cert: fs.readFileSync('cert/cert.pem')
};

// load logger
server.configure(function () {
    server.use(express.logger('dev'));     /* 'default', 'short', 'tiny', 'dev' */
    server.use(express.urlencoded());
	server.use(express.json());
});

//server.use(express.bodyParser());
server.use(express.json());
server.use(express.urlencoded());
//app.use(express.multipart())
//app.use(require('connect-multipart')())

// navigation
server.get('/getUserRegistrationStatus', user.getUserRegistrationStatus);
server.get('/updateUserPosition', user.updateUserPosition);
server.get('/createNewUser', user.createNewUser);
server.get('/getRegisteredPhoneNumbers', user.getRegisteredPhoneNumbers);
server.get('/getPositionOfUser', user.getPositionOfUser);
server.get('/deleteUser', user.deleteUser);
// history
server.get('/insertHistory', user.insertHistory);
server.get('/getHistory', user.getLastHis);
server.get('/getHistoryOfU2', user.getLastHisOfU2);
server.get('/deleteHistory', user.deleteHis);

// download app
server.get('/download', function(req, res){
  var file = 'BuddyNav.apk';
  res.download(file); // Set disposition and send it.
});

// tests
server.get('/test', function(req, res) {
	//console.log("body: " + JSON.stringify(req.body));
	//res.header("Access-Control-Allow-Origin", "*");
	res.send('jsonpCallback({"message":"Hello"})');
});

server.get('/user/:id', user.findById);

// Listen on port 8080, IP defaults to 127.0.0.1
//http.createServer(server).listen(8080);
https.createServer(options, server).listen(8080);

// Put a friendly message on the terminal 
console.log("BuddyNav server running at https://localhost:8080/");