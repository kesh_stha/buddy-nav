//
// source/tutorial:  http://www.hacksparrow.com/using-mysql-with-node-js.html
//
// end connection to db-server: http://stackoverflow.com/questions/14087924/cannot-enqueue-handshake-after-invoking-quit

// database connection variables
var mysql = require('mysql');

var HOST = 'localhost';
var PORT = 3306;
var MYSQL_USER = 'root';
var MYSQL_PASS = 'root';
var DATABASE = 'ajsProjDb';
var TABLE = 'buddy_nav';

// create connection
var connection = mysql.createConnection({
    host: HOST,
    port: PORT,
    user: MYSQL_USER,
    password: MYSQL_PASS,
	database: DATABASE
});

//// KEEP CONNECTION ALIVE ////

function keepalive() {
	connection.query('select 1', [], function(err, result) {
    	if(err) 
			return console.log(err);
  	});
}
setInterval(keepalive, 1000*60*60);


//// NAVIGATION ////

/*
 * returns true or false if the user is already registered
 */
exports.getUserRegistrationStatus = function(req, res) {
	console.log("Get user registration status: " + req.query.phoneNo);
	
	connection.query("select phoneNo from buddy_nav where phoneNo = " + req.query.phoneNo, function(err, results){
		if (err)
			console.log(err);
		else {
			if(results == '' ){
				res.header("Access-Control-Allow-Origin", "*");
				res.send('{"successfully":"false"}');	 
			} else {
				res.header("Access-Control-Allow-Origin", "*");
				res.send('{"successfully":"true"}');
			}
		}

	});
};

/*
 * updates the user position and timestamp
 * returns true or false if the update was successful
 */
exports.updateUserPosition = function(req, res) {
	console.log("Update user position: phoneNo=" + req.query.phoneNo + ", latitude=" + req.query.posLat + ", longitude=" + req.query.posLong + ", timestamp=" + req.query.timestamp);
	
	connection.query("update buddy_nav set posLat = '" + req.query.posLat + "', posLong = '" + req.query.posLong + "', timestamp = " + req.query.timestamp + " where phoneNo = " + req.query.phoneNo + " " , function(err, results){
		if(err) 
			console.log(err);
		else 
			res.send('jsonpCallback({"successfully":"true"})'); 
    });
};

/*
 * creates new user entry including position and timestamp
 * returns true or false if the insert was successful
 */
exports.createNewUser = function(req, res) {
	console.log("Create new user: phoneNo=" + req.query.phoneNo + ", latitude=" + req.query.posLat + ", longitude=" + req.query.posLong + ", timestamp=" + req.query.timestamp);
	
	// delete user with this number 
	connection.query("delete from buddy_nav where phoneNo = " + req.query.phoneNo , function(err, results){
		if(err) {}
    });
	
	connection.query("insert into buddy_nav values ('', '" + req.query.phoneNo + "', '" + req.query.posLat + "', '" + req.query.posLong + "', '" + req.query.timestamp + "')" , function(err, results){
		if(err) 
			console.log(err);
		else 
			res.send('jsonpCallbackReg({"successfully":"true"})'); 
    });
			
    
};

/*
 * returns all of the given phone numbers which are already registered
 */
exports.getRegisteredPhoneNumbers = function(req, res) {
	console.log("Get registered phone numbers");
	
	connection.query("select phoneNo from buddy_nav" , function(err, results){
		if (err)
			console.log(err);
		else {
			res.header("Access-Control-Allow-Origin", "*");
			res.send(JSON.stringify(results));
		}
	});
};

/*
 * returns the position and timestamp of the user
 */
exports.getPositionOfUser = function(req, res) {
	console.log("Get position of user: " + req.query.phoneNo);
	
	connection.query("select posLat, posLong, timestamp from buddy_nav where phoneNo = " + req.query.phoneNo + " LIMIT 1", function(err, results){
		if (err)
			console.log(err);
		else {
			res.header("Access-Control-Allow-Origin", "*");
			res.send(JSON.stringify(results));
		}
	});
};

/*
 * deletes all data of the user
 * returns true or false if delete was successful
 */
exports.deleteUser = function(req, res) {
	console.log("Delete user: " + req.query.phoneNo);
	
	connection.query("delete from buddy_nav where phoneNo = " + req.query.phoneNo , function(err, results){
		if(err) 
			console.log(err);
		else
			res.send('jsonpCallbackDel({"successfully":"true"})'); 
    });
}

//// HISTORY TABLE ////

/*
 * creates entry into HISTORY table
 * returns true or false if delete was successful
 */
exports.insertHistory = function(req, res) {
	console.log("Insert history for user: " + req.query.phoneNo);
	
	connection.query("insert into history values (''," + req.query.phoneNo + ", " + req.query.posLat + ", " + req.query.posLong + ", " + req.query.u2phoneNo + ", " + req.query.u2posLat + ", " + req.query.u2posLong + ", " + req.query.timestamp + ")" , function(err, results){
		if(err) 
			console.log(err);
		else 
			res.send('jsonpCallback({"successfully":"true"})'); 
    });
}

/*
 * filtering result, showing last 5 records
 */
exports.getLastHis = function(req, res) {
	console.log("Get history for user: " + req.query.phoneNo);
	
	connection.query("SELECT * FROM history WHERE phoneNo = " + req.query.phoneNo + " ORDER BY timestamp DESC LIMIT 5" , function(err, results){
		if(err) 
			console.log(err);
		else {
			res.header("Access-Control-Allow-Origin", "*");
			res.send(JSON.stringify(results));
		}
	});
}


/*
 * filtering result, showing last 5 records for second user
 */
exports.getLastHisForSecUser = function(req, res) {
	console.log("Get history for user: " + req.query.u2phoneNo);
	
	connection.query("SELECT * FROM history WHERE u2phoneNo = " + req.query.u2phoneNo + " ORDER BY timestamp DESC LIMIT 5" , function(err, results){
		if(err) 
			console.log(err);
		else {
			res.header("Access-Control-Allow-Origin", "*");
			res.send(JSON.stringify(results));
		}
	});
}

/*
 * deletes history
 */
exports.deleteHis = function(req, res) {
	console.log("Delete history for user: " + req.query.phoneNo);
	
	connection.query("DELETE FROM history WHERE phoneNo = " + req.query.phoneNo + " AND id < (SELECT MIN(id) FROM (SELECT * FROM history WHERE phoneNo = " + req.query.phoneNo + " ORDER BY id DESC LIMIT 5) AS COUNT)" , function(err, results){
		if(err) 
			console.log(err);
		else
			res.send('jsonpCallbackDelHis({"successfully":"true"})'); 
	});
}


//// TESTS ////
exports.getAllUsers = function(req, res) {
    res.send([{name:'user1'}, {name:'user2'}, {name:'user3'}]);
};
 
exports.findById = function(req, res) {
    res.send({id:req.query.id, name: "The Name", description: "description"});
};