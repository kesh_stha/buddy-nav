/*
 * load class for database connection
 */
dbConnection = require('./dbConnection');

/*
 * load class for phone number formation
 */
phoneNo = require('./phoneNumber');

/*
 * returns true or false if the user is already registered
 */
exports.getUserRegistrationStatus = function(req, res) {
	dbConnection.getUserRegistrationStatus(req, res);
};

/*
 * updates the user position and timestamp
 */
exports.updateUserPosition = function(req, res) {
	
	// check if a number was transmitted
	if( req.query.phoneNo != null &&  req.query.phoneNo.length > 0){
		dbConnection.updateUserPosition(req, res);
	}
};

/*
 * creates new user entry
 */
exports.createNewUser = function(req, res) {
	
	dbConnection.createNewUser(req, res);
};

/*
 * returns all of the given phone numbers which are already registered
 */
exports.getRegisteredPhoneNumbers = function(req, res) {
	dbConnection.getRegisteredPhoneNumbers(req, res);
};

/*
 * returns the position of the user
 */
exports.getPositionOfUser = function(req, res) {
	
	// delete spaces
	req.query.phoneNo = req.query.phoneNo.replace(" ", "");
	
    dbConnection.getPositionOfUser(req, res);
};

/*
 * deletes all data of the user
 */
exports.deleteUser = function(req, res) {
    dbConnection.deleteUser(req, res);
}

/*
 * creates entry into HISTORY table
 */
exports.insertHistory = function(req, res) {
    dbConnection.insertHistory(req, res);
}

/*
 * filtering result, showing last 5 records
 */
exports.getLastHis = function(req, res) {
    dbConnection.getLastHis(req, res);
}

/*
 * filtering result, showing last 5 records
 */
exports.getLastHisOfU2 = function(req, res) {
    dbConnection.getLastHisForSecUser(req, res);
}

/*
 * deletes history
 */
exports.deleteHis = function(req, res) {
    dbConnection.deleteHis(req, res);
}

////////////////////////////////////////////// tests
exports.getAllUsers = function(req, res) {
    dbConnection.getAllUsers(req, res);
};
 
exports.findById = function(req, res) {
    dbConnection.findById(req, res);
};