/*
 * formats phone number
 * e.g.: 
 * +491736696720 -> +491736696720
 *   01736696720 -> +XX1736696720
 */
exports.format = function(no) {
	if(no.substr(0, 1) == "+") {
		return no;
	} else {
		return "+XX".concat(no.substr(1, no.length));
	}
};