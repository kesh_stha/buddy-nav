/*
 * update coordinates
 */
function updateCoords(phoneNo, posLat, posLong, timestamp){
		
	//alert("updating: " + phoneNo + " " + posLat + " " + posLong + " " + timestamp);
		
	var data = {};
	data.phoneNo = phoneNo;
	data.posLat = posLat;
	data.posLong = posLong;
	data.timestamp = timestamp;
	
	$.ajax({
		type: 'GET',
		url: 'https://ajs.jumpingcrab.com:8080/updateUserPosition',
		dataType: "jsonp",
		data: data,
		crossDomain:true,
		jsonpCallback: '',
		contentType: "application/json",
		cache: false,
		timeout: 5000,
		success: function(data) {
           jsonpCallback(data)
        }
	});
};

function jsonpCallback(data) {
	//alert('update cords');
}
