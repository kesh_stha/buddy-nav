// JavaScript Document

//$(document).ready(function(){
	//	$("button").click(function(){
		
/*
$.each( data, function( key, val ) {
	alert( "posLat: " + val.posLat);
});
*/

/*
 * registers the user
 */
function registerUser(number){
			
	var data = {};
	data.phoneNo = number;
	data.posLat = 0;
	data.posLong = 0;
	data.timestamp = 0;
	
	$.ajax({
		type: 'GET',
		url: 'https://ajs.jumpingcrab.com:8080/createNewUser',
		dataType: "jsonp",
		data: data,
		crossDomain:true,
		jsonpCallback: '',
		contentType: "application/json",
		cache: false,
		timeout: 5000,
		//success: jsonpCallback
		success: function(data) {
            jsonpCallbackReg(data);
        }
	});
};

function jsonpCallbackReg(data){
	//alert(data.successfully);
	//alert("Lat: " + data[0].posLat + " Long: " + data[0].posLong);
}

/*
 * get position of the user
 */
function getPositionOfUser(number, callback){
			
	var data = {};
	data.phoneNo = number;
	
	$.ajax({
		type: 'GET',
		url: 'https://ajs.jumpingcrab.com:8080/getPositionOfUser',
		dataType: "jsonp",
		data: data,
		crossDomain:true,
		jsonpCallback: '',
		contentType: "application/json",
		cache: false,
		timeout: 5000,
		success: function(data) {
			jsonpCallbackPos(data);
		}
	});
};

function jsonpCallbackPos(data) {
	alert(data[0].posLat);
}

/*
 * deletes the user
 */
function deleteUserInDb(number){
			
	var data = {};
	data.phoneNo = number;
	
	$.ajax({
		type: 'GET',
		url: 'https://ajs.jumpingcrab.com:8080/deleteUser',
		dataType: "jsonp",
		data: data,
		crossDomain:true,
		jsonpCallback: '',
		contentType: "application/json",
		cache: false,
		timeout: 5000,
		//success: jsonpCallback
		success: function(data) {
            jsonpCallbackDel(data);
        }
	});
};

function jsonpCallbackDel(data){
	alert("User deleted!");
}

function insertNavHistory(details) {
	
	$.ajax({
		type: 'GET',
		url: 'https://ajs.jumpingcrab.com:8080/insertHistory',
		dataType: "jsonp",
		data: details,
		crossDomain:true,
		jsonpCallback: '',
		contentType: "application/json",
		cache: false,
		timeout: 5000,
		success: function(data) {
		   jsonpCallback(data);
		},
		error: function(err) {
			jsonpCallback(err);
		}
	});
}

function jsonpCallback(data) {
	//alert("History updated");
}

function deleteHistory(number) {
	var data = {};
	data.phoneNo = number;
	
	$.ajax({
		type: 'GET',
		url: 'https://ajs.jumpingcrab.com:8080/deleteHistory',
		dataType: "jsonp",
		data: data,
		crossDomain:true,
		jsonpCallback: '',
		contentType: "application/json",
		cache: false,
		timeout: 5000,
		//success: jsonpCallback
		success: function(data) {
            jsonpCallbackDelHis(data);
        }
	});
}

function jsonpCallbackDelHis(data){
	//alert("History updated (del)");
}
