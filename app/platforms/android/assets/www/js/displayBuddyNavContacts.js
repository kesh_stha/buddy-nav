$(document).ready// get phone number from GET parameter
var str = window.location.search.substring(1).split('&');
var t = str.toString();
phoneNo = t.substring((t.indexOf("=")+1), t.length);

//alert(phoneNo);

// update coordinates every 1 min
setInterval( function() { 		
	navigator.geolocation.getCurrentPosition(
		function(pos) {
			var userLatlng = new google.maps.LatLng(pos.coords.latitude, pos.coords.longitude); 
			updateCoords(phoneNo, pos.coords.latitude, pos.coords.longitude, Math.round((new Date()).getTime() / 1000));
		},
		function(error) { //onError               
			alert('code: '    + error.code    + '\n' +    'message: ' + error.message);
		}, 
		{
			frequency: 3000,
			enableHighAccuracy: true
		}
	);
}, 1000*60);      

document.addEventListener("deviceready", getContactList, false);
function getContactList() {
	navigator.contacts.find(['displayName', 'name','phoneNumbers'],
		function(contacts){
			var myContacts = [];
			$.getJSON('https://ajs.jumpingcrab.com:8080/getRegisteredPhoneNumbers', function(data){
				for (var i=0; i<contacts.length; i++) {
					if(contacts[i].phoneNumbers != null && contacts[i].phoneNumbers.length > 0 &&
						contacts[i].phoneNumbers[0].value != null && 
						contacts[i].phoneNumbers[0].value != undefined) {
						var phoneNumber = contacts[i].phoneNumbers[0].value; 
					}
					else {
						phoneNumber = "--No Number--";
					}
					
					if(phoneNumber.charAt(0) == '+'){
						phoneNumber = phoneNumber.substring(3);
					}
					else if(phoneNumber.charAt(0) == 0){
						phoneNumber = phoneNumber.substring(1);
					}
					phoneNumber = phoneNumber.replace(/ /g, '');
					var name = contacts[i].displayName != null ? contacts[i].displayName: "No name";
					for(var index in data) {
						if(data[index].phoneNo == phoneNumber){
							myContacts.push({"name":name, "phoneNumber": phoneNumber} );							
							$("#list-contact").append ( "<li><a href='frenlocation.html?phoneNo=" + parsePhoneNo(phoneNumber) + "' rel='external' >" + name + "</a></li>");
						}
					}
				}
				displayHistory(myContacts);	// display history with names
			});        
		},
		function(error){
			alert(error);
		},
		{ filter:"", multiple:true }
	);
}

function sortNames(a, b ) {
	return a.name.toLowerCase() > b.name.toLowerCase() ? 1 : -1;
}
		   
function parsePhoneNo(phoneNo){
	if(phoneNo.substring(0, 1) == "0"){
		return phoneNo.substring(1, phoneNo.lenght);
	} else if (phoneNo.substring(0, 3) == "+49"){
		return phoneNo.substring(3, phoneNo.lenght)
	} else {
		return phoneNo;
	}
}

function askForCoordinates(number) {
	getPositionOfUser(number);	
}		