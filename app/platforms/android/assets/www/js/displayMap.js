var get_api_url = "https://ajs.jumpingcrab.com:8080/";

var commonFunc = {
	getPhoneNumber: function() {
		var str = window.location.search.substring(1).split('&');
		var t = str.toString();
		return t.substring((t.indexOf("=")+1), t.length);
	}
}

var routemap = {
    // Application Constructor
    initialize: function() {
        this.bindEvents();
    },
    
    // Bind any events that are required on startup. Common events are:
    // 'load', 'deviceready', 'offline', and 'online'.
    bindEvents: function() {
        document.addEventListener('deviceready', this.onDeviceReady, false);
    },
    // deviceready Event Handler
    //
    // The scope of 'this' is the event. In order to call the 'receivedEvent'
    // function, we must explicity call 'routemap.receivedEvent(...);'
    onDeviceReady: function() {     
		
		//alert(commonFunc.getPhoneNumber());
		routemap.getRoute(commonFunc.getPhoneNumber());
    },
	
	//Google map integration
	googleMap: function(id, centerPos) {
		var mapOptions = {
			zoom: 15,
			center: centerPos,//new google.maps.LatLng(54.315504, 10.130652),
			mapTypeId: google.maps.MapTypeId.ROADMAP
		}
		 
		var map = new google.maps.Map(document.getElementById(id), mapOptions);
		 
		return map;
	},
	
	//Show route between two points
	showRoute: function(map, from, to) {
		var directionsService = new google.maps.DirectionsService();
		var directionOptions = {
			origin: from,
			destination: to,
			travelMode: google.maps.DirectionsTravelMode.DRIVING,
			unitSystem: google.maps.UnitSystem.METRIC
		};
		directionsService.route(
			directionOptions,
			function(response, status) {
				if(status == google.maps.DirectionsStatus.OK) {
					new google.maps.DirectionsRenderer({
						map: map,
						directions: response
					});
				}
				else {
				
				}
			}
		);		
	},
	
	//Gets your location //'Kiel Hbf, Kiel'
	getRoute: function(fren_id) {
		
		isHistory = false;
        watchID = navigator.geolocation.watchPosition(
			function(position) { //onSuccess		
				
				$.getJSON(get_api_url+'getPositionOfUser?phoneNo='+fren_id,function(data) { // Get destination co-ordinates
					
					//alert('My pos: '+ position.coords.latitude+' '+ position.coords.longitude);
					//alert('Fren pos: '+data[0].posLat+' '+ data[0].posLong);
					//alert(localStorage.getItem("phoneNo"));
					//alert(isHistory);
										
					var map = routemap.googleMap('map_canvas', new google.maps.LatLng(position.coords.latitude, position.coords.longitude));
					routemap.showRoute(map, 
						new google.maps.LatLng(position.coords.latitude, position.coords.longitude), 
						new google.maps.LatLng(data[0].posLat, data[0].posLong));					
					
					if(!isHistory) {
						user_phoneNo = localStorage.getItem("phoneNo");
						var historyDetails = {};
						// here goes data
						historyDetails.phoneNo = user_phoneNo;
						historyDetails.posLat = position.coords.latitude;
						historyDetails.posLong = position.coords.longitude;
						historyDetails.u2phoneNo = fren_id;
						historyDetails.u2posLat = data[0].posLat;
						historyDetails.u2posLong = data[0].posLong;
						historyDetails.timestamp = Math.round(new Date().getTime()/1000);
													
						//alert("here history " + new Date().getTime());
						insertNavHistory(historyDetails);
						deleteHistory(user_phoneNo);
						isHistory = true;
					}					
				});				
			},
			function(error) { //onError				
				alert('code: '    + error.code    + '\n' +	'message: ' + error.message);
				/*element.innerHTML = 'code: '    + error.code    + '\n' +
									'message: ' + error.message;*/
			}, 
			{ //options
				frequency: 3000,
				enableHighAccuracy: true				
			}
		);		
		
	}
};

var frenpos = {
	// Application Constructor
    initialize: function() {
        this.bindEvents();
    },
    
    // Bind any events that are required on startup. Common events are:
    // 'load', 'deviceready', 'offline', and 'online'.
    bindEvents: function() {
        document.addEventListener('deviceready', this.onDeviceReady, false);
    },
    // deviceready Event Handler
    //
    // The scope of 'this' is the event. In order to call the 'receivedEvent'
    onDeviceReady: function() {     
		//alert(commonFunc.getPhoneNumber());	
		var map = frenpos.showMarker(commonFunc.getPhoneNumber());		
    },	
	
	//Google map integration
	showMarker: function(fren_id) {
	
		$.getJSON(get_api_url + 'getPositionOfUser?phoneNo='+fren_id, function (data) {
			
			//alert(data[0].posLat + ' ' + data[0].posLong);
			
			$('a#navigate-btn').attr("href", $('a#navigate-btn').attr("href") + "?phoneNo="+fren_id);
						
			//alert($('a#navigate-btn').attr("href"));
		
			var userLatlng = new google.maps.LatLng(data[0].posLat, data[0].posLong);//new google.maps.LatLng(pos.coords.latitude, pos.coords.longitude);
			
			var mapOptions = {
				zoom: 15,
				center: userLatlng,
				mapTypeId: google.maps.MapTypeId.ROADMAP
			}
			 
			var map = new google.maps.Map(document.getElementById('map_canvas'), mapOptions);
			 
			var marker = new google.maps.Marker({
				position: userLatlng,
				map: map,
				title:"Here"
			});				
		});			
	}
};