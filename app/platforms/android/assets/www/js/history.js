user_phoneNo = localStorage.getItem("phoneNo");

function convertDate(timestamp) {
	return new Date(timestamp*1000)
	/*var date = new Date(timestamp*1000);
	var iso = date.toISOString().match(/(\d{2}:\d{2}:\d{2})/)
	return iso[0] + " " + iso[1];*/
}

function getNameOfNumber(number, myContacts){

	//alert(number + " " + myContacts[1].phoneNumber)
	
	for(var i=0; i<myContacts.length; i++){
		if(myContacts[i].phoneNumber == number){
			return myContacts[i].name;
		}
	}
	return "No Name";
	
}

function displayHistory(myContacts){

	// Get history - who has navigated you
	$.getJSON('https://ajs.jumpingcrab.com:8080/getHistoryOfU2?u2phoneNo='+user_phoneNo, function(data) {
	
		$.each(data, function(index, obj) {
		
			$("#list-history").append('<li class="outgoing">' +
					'<a href="#" rel="external" class="ui-btn ui-btn-inline" data-role="button" data-transition="slide">' +
						'<img src="img/default-image-2.png" alt="Profile Picture Default">' +
						'<h2>'+getNameOfNumber(obj["u2phoneNo"], myContacts)+'</h2>' +
						'<p>'+convertDate(obj["timestamp"]) +'</p></a>' +
					'</a>' +
				'</li>');
				
		});			
	});
	
	// Get history - who you have navigated
	$.getJSON('https://ajs.jumpingcrab.com:8080/getHistory?phoneNo='+user_phoneNo, function(data) {
	
		$.each(data, function(index, obj) {
		
			$("#list-history").append('<li class="incoming">' +
					'<a href="#" rel="external" class="ui-btn ui-btn-inline" data-role="button" data-transition="slide">' +
						'<img src="img/default-image-1.png" alt="Profile Picture Default">' +
						'<h2>'+getNameOfNumber(obj["u2phoneNo"], myContacts)+'</h2>' +
						'<p>'+convertDate(obj["timestamp"])+'</p></a>' +
					'</a>' +
				'</li>');
				
		});			
	});
}