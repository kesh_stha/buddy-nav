		
$(document).ready(function(){

	/* Home Page Register Button enable function */
	$('#home .phoneNumberError p').css('display', 'none');
	$('#home .middle-text a').css('pointer-events', 'none');
	$('#phone-number').on('focusout', function() {
		var notempty = $(this).val();
		if (!notempty) {
			$(this).addClass('no-shadow').removeClass('error-shadow success-shadow');
		}
	});

	$('#phone-number').on('keyup', function() {
		
		var context   = $('#home .middle-text');
		var $self     = $(this);
		var validator = $self.val();
		
		$('a', context).css('pointer-events', 'none');
		$(this).attr('data-icon','alert');
		$(this).addClass('ui-icon-alert error-shadow');
		$('.phoneNumberError p', context).css('display', 'block');
		
		if (isNumber(validator)) {
			if(validator.length == 10 || validator.length == 11 ) {
				
				if(validator.charAt(0) === '1') {
					$('a', context).css({
							'pointer-events': 'auto',
							'cursor': 'pointer'
					});   
					$(this).attr('data-icon','check');
					$(this).removeClass('ui-icon-alert error-shadow').addClass('ui-icon-check success-shadow');
					$('.phoneNumberError p', context).hide().fade(800);
				}
					
			} else {
				$('a', context).css({
						'pointer-events': 'none',
						'cursor': 'pointer'
				});   
				$(this).attr('data-icon','alert');
				$(this).removeClass('ui-icon-check success-shadow').addClass('ui-icon-alert error-shadow');
				$('.phoneNumberError p', context).css('display', 'none');
			}
		
		}
		
	});
	
	/* Changes the pages */
	
	$('.top-header li').on('click', function () {
		//alert('hello');
		var hrefValue = $(this).children().attr('href');
		//alert(hrefValue);
		$.mobile.changePage( hrefValue, { transition: "slide"} );
		
	});
	
});
	
function isNumber(n) {
	return !isNaN(parseFloat(n)) && isFinite(n);
}

$.fn.placeholder = function() {

        this.each(function() {
                $(this).data('placeholder', new Placeholder(this));
        });
        
        return this;
};

var Placeholder = function(element) {
        this.$element = $(element);
        this._init();
};

Placeholder.prototype = {
    
	_init: function() {
	   
		var self = this;
		
		self.$element.each(function() {
																
			$(self.$element).on('focus', function() {
				$(this).data('placeholder', $(this).attr('placeholder')); 
				$(this).removeAttr('placeholder');
			});
	
			$(self.$element).on('focusout', function()	{
				$(this).attr('placeholder', $(this).data('placeholder'));
			});
			
		});
		
	}  
};

$(document).on('pageshow', function() {
    
	var activepage =$.mobile.activePage.attr('id');
	//alert(activepage);
    $(this).removeClass('ui-btn-active');
    switch(activepage) {
        /*
		case 'contact-listing' :
            //alert('#' + activepage + ' li.ui-block-a a');
			//$('#recent-contacts li.ui-block-b a', '#contact-listing li.ui-block-c a').removeClass('ui-btn-active');
            $('#' + activepage + ' li.ui-block-a a', '#' + activepage + ' li.ui-block-a').children().children().next().addClass('ui-btn-active');
            break;
            
        case 'recent-contacts' :
            //alert('#' + activepage + ' li.ui-block-b a');
			//$('#contact-listing ui-block-a a', '#contact-listing li.ui-block-c a').removeClass('ui-btn-active');
            // $('#' + activepage + ' li.ui-block-b a', '#' + activepage + ' li.ui-block-b').children().children().next().addClass('ui-btn-active');
            // break;
			
			// text($(this).text().substr(0, 35))
			// $('#' + activepage + ' #list-view li p').substring(0, 24);
			break;
        /*    
         case 'settings' :
            //alert('#' + activepage + ' li.ui-block-c a');
			//$('#recent-contacts li.ui-block-b a', '#contact-listing li.ui-block-a a').removeClass('ui-btn-active');
            $('#' + activepage + ' li.ui-block-c a', '#' + activepage + ' li.ui-block-c').children().children().next().addClass('ui-btn-active');
            break;
			
			*/
			
		case 'navigate-to-user' :
			fitHeight('#'+activepage);
			break;
			
		case 'map-navigation' :
			fitHeight('#'+activepage);
			break;
    }
	
});

function fitHeight(id) {
	//alert(id);
	//alert('Viewpoint height' + $(window).height());
	//alert('header height' + $(id + ' .top-header').height());
	
	var header_height = $(id + ' .top-header').height();
	var viewpoint_height = $(window).height();
	var fifth_height = viewpoint_height - round(viewpoint_height * 0.10); //10% of viewpoint
	var map_height = viewpoint_height - header_height - fifth_height;
	
	switch(id) {
		case '#map-navigation' :
			//alert(id);
			var lower_button = $(id + '.navigate-buttons').height();
			map_height = map_height - lower_button;
			//alert('map-height' + map_height);			
			$('#map-canvas img').css('height' , map_height);
			break;
			
		case '#navigate-to-user' :
			//alert(id);
			$('#full-map img').css('height' , map_height);
			break;
	}
	
}

